# Toolkits Manager CLI

Toolkits Manager CLI is an Bash-only shell script to managing
manually-installed tools without messing around your system packages.

Efforts to make this CLI more POSIX-friendly and even porting
the scripts to POSIX-compliant format are welcome!

## Installing

```bash
# if forked Andrei Jiroh's dotfiles repo or using The Pins Team's
# dotfiles template, you probably don't need to run this.
# either the bootstraper script (or the usual ./setup.sh file
# should be fine). if too outdated, see UPGRADING.md on
# upstream repo.
$(command -v curl>>/dev/null && echo "curl -sL" || echo "wget -0-") https://scripts.madebythepins.tk/toolkits-manager-cli/install | bash -

# if scripts.madebythepins.tk is down, you can also directly use
# the installer script from this repo, directly.
# (SPOILER: That subdomaon doesn't exists yet)
$(command -v curl>>/dev/null && echo "curl -sL" || echo "wget -0-") https://gitlab.com/toolkit-mgr/cli/-/raw/main | bash -
```

### Upgrading

Upgrading is easy as doing `git pull origin main` to retrive the
latest development commit or by tag name with our own script with
`toolkit-mgr run upgrade-toolkit-mgr`.

## Configuring your install

Duplicate `~/.toolkits-mgr/default-config` as `~/.toolkits-mgr.config`
and that config file is well documentated. Please read the comments before
doing anything. Config changes are applied on next invocation
of `toolkit-mgr`.

The config file is an INI file

## FAQs

### Where the included tools can be found?

It's always on `~/.toolkits-mgr/included-tools`, and if ran
`toolkit-mgr uninstall <included-tool-name>`, it'll just error out,
sayong that removing an included tool may break your Toolkits Manager
installation.

### Mom, I broke my Toolkits Mamager installation. Welp!

In case of broken Toolkit Manager install, you should:

* **update to the latest release with `toollit-mgr upgrade toolkit-mgr`**.
This process will fetch for available stable versions and pull the latest
one. If you need the latest development stuff, just do
`git -C $TOOLKITS_MGR_HOME switch main && git -C $TOOLKITS_MGR_HOME pull origin main`.
* **start over again, if things go wrong.** Please backup your installed
packages' files and index before proceeding. Please also backup

## License

TODO
